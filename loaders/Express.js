const bodyParser = require('body-parser');
const express = require('express');
const cors = require('cors');
const path = require('path');
const compression = require('compression');
const routes = require('../routes');

// API port
const { PORT } = require('../config');

class ExpressLoader {
  constructor() {
    const app = express();

    // CORS
    app.use(cors());

    // Serve static content
    app.use(express.static(path.join(__dirname, "uploads")));

    // Middleware
    app.use(compression());
    app.use(bodyParser.urlencoded({
      extended: false,
      limit: "20mb"
    }));
    app.use(bodyParser.json({limit: "20mb"}));

    // Routes
    routes(app);

    // Error handling
    require('../middleware/errMid')(app);

    // Start application
    this.server = app.listen(PORT, () => {
      console.log(`Express is listening on port ${PORT}`);
    });
  }
}

module.exports = ExpressLoader;