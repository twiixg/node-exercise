const peopleRoute = require('./people');
const planetsRoute = require('./planets');

const routes = app => {

  app.use("/people", peopleRoute);
  app.use("/planets", planetsRoute);

  // The 404 Route
  app.get('*', function(req, res, next) {
    let err = new Error('Not found');
    err.statusCode = 404;
    return next(err);
  });
}

module.exports = routes;