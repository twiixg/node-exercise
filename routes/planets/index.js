const express = require('express');
const router = express.Router();

const PlanetsService = require('../../services/PlanetsService');
const planetsService = new PlanetsService();

const PeopleService = require('../../services/PeopleService');
const peopleService = new PeopleService();

/**
 * GET /planets
 */
router.get('/', async (req, res, next) => {
  let response = {
    error: null,
    data: null
  }

  let status = 500;

  try {
    let planetsResult = await planetsService.getAllPlanets();

    if (planetsResult.error) throw Error(planetsResult.error);

    let planets = planetsResult.data;

    // Convert residents for each planet
    if (planets) {
      for (let idx in planets) {
        let planet = planets[idx];
        if (!planet.residents) continue;

        // Get residents names, replace original residents array
        planets[idx].residents = await Promise.all(
           planet.residents.map(async personUrl => {
            return peopleService.getPersonNameByUrl(personUrl);
          })
        );
      }
    }

    response.data = planets;
    status = 200;

  } catch (err) {
    err.statusCode = status;
    return next(err);
  }

  return res.status(status).json(response);
});

module.exports = router;