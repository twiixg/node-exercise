const express = require('express');
const router = express.Router();

const PeopleService = require('../../services/PeopleService');
const peopleService = new PeopleService();

/**
 * GET /people
 */
router.get('/', async (req, res, next) => {
  let response = {
    error: null,
    data: null
  }

  // Get sortBy value
  let sortBy = req.query.sortBy ?? null;

  // Set default response status
  let status = 500;

  try {
    let peopleResult = await peopleService.getAllPeople();
    if (peopleResult.error) throw Error(peopleResult.error);

    response.data = peopleResult.data;

    if (sortBy) {
      response.data = peopleService.sort(response.data, sortBy);
    }

    status = 200;

  } catch (err) {
    err.statusCode = status;
    return next(err);
  }

  return res.status(status).json(response);
});

module.exports = router;