const axios = require('axios');
const { SW_API_URL } = require('../config');

class SwApiRequestService {
  baseUrl = SW_API_URL;

  // TODO: request caching layer, for example Redis

  /**
   * GET request with a primitive caching
   * @param {String} url
   * @return {Promise<{data, error: <String|null>}>}
   */
  async getRequest(url) {
    let result = {
      error: null,
      data: null
    }

    if (!url) return result;

    try {
      let response = await axios.get(url);
      result.data = response.data;
    } catch (err) {
      result.error = err.message;
    }

    return result;
  }

  /**
   * Get all objects using swapi pagination
   * @param {String} firstPageUrl
   * @return {Promise<{data: <Array|null>, error: <String|null>}>}
   */
  async getAll(firstPageUrl) {
    let result = {
      error: null,
      data: null
    }

    let url = firstPageUrl;
    let data = [];

    while(url) {
      let response = await this.getRequest(url);
      if (response.error) {
        result.error = response.error;
        return result;
      }

      data = [...data, ...response.data.results];
      url = response.data.next;
    }

    result.data = data;
    return result;
  }
}

module.exports = SwApiRequestService;