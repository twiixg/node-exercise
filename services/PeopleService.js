const SwApiRequestService = require('./SwApiRequestService');

class PeopleService extends SwApiRequestService {
  // Api Endpoint
  apiEndPoint = '/people';

  // Allowed sorting keys
  sortFieldsMethods = {
    name: '_cmpStr',
    height: '_cmpNum',
    mass: '_cmpMass'
  };

  /**
   * Get array of all people
   * @return {Promise<{data: <null|Array>, error: <null|String>}>}
   */
  async getAllPeople() {
    return this.getAll(`${this.baseUrl}${this.apiEndPoint}`);
  }

  /**
   * Get persons data by its API URL
   * @param {String} url
   * @return {Promise<{data: <null|Object>, error: <null|String>}>}
   */
  async getPersonByUrl(url) {
    let result = {
      error: null,
      data: null
    }

    let response = await this.getRequest(url);
    if (response.error) {
      result.error = response.error;
    } else {
      result.data = response.data;
    }

    return result;
  }

  /**
   * Get name of the person by its API URL
   * @param {String} url
   * @return {Promise<null|String>}
   */
  async getPersonNameByUrl(url) {
    let personResult = await this.getPersonByUrl(url)
    if (personResult.error) return null;

    return personResult.data.name;
  }

  /**
   * Sort array of people by key
   * @param {Object[]} data
   * @param {String} key
   * @return {null|*}
   */
  sort(data, key) {
    if (!Array.isArray(data)) return null;

    let cmpFunc = this.sortFieldsMethods[key];
    if (!cmpFunc) return data;

    data.sort((a, b) => this[cmpFunc](a, b, key));

    return data;
  }

  _cmpStr(a, b, key) {
    let strA = a[key].toUpperCase(); // ignore upper and lowercase
    let strB = b[key].toUpperCase(); // ignore upper and lowercase
    if (strA < strB) {
      return -1;
    }
    if (strA > strB) {
      return 1;
    }
    return 0;
  }

  _cmpMass(a, b, key) {
    let massA = a[key];
    let massB = b[key];

    if (massA === "unknown" || massB === "unknown") {
      if (massA === "unknown") {
        return -1;
      } else if (massB === "unknown") {
        return 1;
      } else {
        return 0;
      }
    }

    return parseFloat(a[key]) - parseFloat(b[key]);
  }

  _cmpNum(a, b, key) {
    return parseFloat(a[key]) - parseFloat(b[key]);
  }
}

module.exports = PeopleService;