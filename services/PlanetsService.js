const SwApiRequestService = require('./SwApiRequestService');

class PlanetsService extends SwApiRequestService {
  apiEndPoint = '/planets';

  /**
   * Get array of all planets
   * @return {Promise<{data: <null|Array>, error: <null|String>}>}
   */
  async getAllPlanets() {
    return this.getAll(`${this.baseUrl}${this.apiEndPoint}`);
  }
}

module.exports = PlanetsService;