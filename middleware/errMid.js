/**
 * Errors handling middleware
 * @param app
 */
module.exports = function(app) {
  app.use(function (err, req, res, next) {

    let response = {
      data: null,
      error: err.message
    }
    console.error(err);

    // TODO: Log errors

    return res.status(err.statusCode ?? 500).json(response);
  });
}