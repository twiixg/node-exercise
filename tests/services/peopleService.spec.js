const PeopleService = require('../../services/PeopleService');
const peopleService = new PeopleService();
const data = [
  {
    "name": "Biggs Darklighter",
    "height": "183",
    "mass": "84",
  },
  {
    "name": "Ackbar",
    "height": "180",
    "mass": "83",
  },
  {
    "name": "Ki-Adi-Mundi",
    "height": "198",
    "mass": "unknown",
  }
];

describe("PeopleService", () => {
  describe("PeopleService.sort", () => {
    test("no data", () => {
      const result = peopleService.sort(null, '');
      expect(result).toBe(null);
    });

    test("not allowed sorting key", () => {
      const result = peopleService.sort(data, 'abc');
      expect(result).toEqual(data);
    });

    test("sort by height", () => {

      const result = peopleService.sort(data, 'height');

      expect(result).toEqual(
        [
          expect.objectContaining({height: "180"}),
          expect.objectContaining({height: "183"}),
          expect.objectContaining({height: "198"})
        ]
      );
    });

    test("sort by mass", () => {

      const result = peopleService.sort(data, 'mass');

      expect(result).toEqual(
        [
          expect.objectContaining({mass: "unknown"}),
          expect.objectContaining({mass: "83"}),
          expect.objectContaining({mass: "84"})
        ]
      );
    });

    test("sort by name", () => {

      const result = peopleService.sort(data, 'name');

      expect(result).toEqual(
        [
          expect.objectContaining({height: "180"}),
          expect.objectContaining({height: "183"}),
          expect.objectContaining({height: "198"})
        ]
      );
    });
  });
})