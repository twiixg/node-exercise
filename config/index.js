const mode = process.env.MODE === "production" ? process.env.MODE : "development";

const config = {};

config.production = {
  PORT: 3000,
  SW_API_URL: "https://swapi.dev/api"
}

config.development = {
  PORT: 3000,
  SW_API_URL: "https://swapi.dev/api"
}

module.exports = config[mode];